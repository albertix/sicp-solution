(defproject sicp-solutions "1.0"
  :license {:name "GNU Lesser General Public License"
            :url "http://www.gnu.org/copyleft/lesser.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]])
